# Contribution Guide

Thank you for your interest in contributing to Seinfeld Locations! Giddy up! The
following are instructions on how you can contribute to the project.

## Add or Correct a Location

If you want to add a new location, or make a correction for a location listed on
the map, please submit a new issue with the name of the location, address and or
coordinates (latitude and longitude) if possible, and the season(s) and
episode(s) of _Seinfeld_ where this location appears.

You can also add or correct the location in `locations` variable in `src/app.js`
file following the same structure of the other locations. Then create a new
merge request for your change mentioning the issue or describing the location as
explained above for issues. Currently, all locations are hardcoded. In the near
future it will be changed to make the locations management more dynamic.

## Suggest a New Feature

If you have an idea for a new feature, please submit a new issue describing it.
Merge requests with your new feature and its detailed description, or mentioning
an existing issue, are welcome too.

## Report a Bug

Bugs can appear, unfortunately. When you find one, please submit a new issue
with its description. Describe the steps to reproduce the bug if possible and
the browser and version of the browser used when the bug was found. The more
details the better.

Again, merge requests are welcome. Describe the bug and its correction or
mention the issue number of the bug on the merge request description.

## Yada yada yada

For any other suggestions, complaints, questions, please submit a new issue. We
will be very happy to hear you!
