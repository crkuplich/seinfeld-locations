/**
 * Copyright 2017 Cassiano Rocha Kuplich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var gulp = require('gulp');
var del = require('del');
var browserSync = require('browser-sync').create();
var useref = require('gulp-useref');
var gulpIf = require('gulp-if');
var csso = require('gulp-csso');
var uglify = require('gulp-uglify');
var critical = require('critical').stream;
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');

gulp.task('clean', function() {
  return del('public');
});

gulp.task('serve', function() {
  browserSync.init({
    server: {
      baseDir: 'src'
    }
  });
});

gulp.task('serve:public', ['build'], function() {
  browserSync.init({
    notify: false,
    server: {
      baseDir: 'public'
    }
  });
});

gulp.task('watch', ['serve'], function() {
  gulp.watch('src/**/*', browserSync.reload);
});

gulp.task('html', function() {
  return gulp.src('src/index.html')
    .pipe(useref())
    .pipe(gulpIf('*.css', csso()))
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulp.dest('public'));
});

gulp.task('html:critical', ['html'], function() {
  return gulp.src('public/index.html')
    .pipe(critical({
      base: '.',
      inline: true,
      minify: true,
      width: 320,
      height: 480
    }))
    .pipe(htmlmin({
      collapseWhitespace: true
    }))
    .pipe(gulp.dest('public'));
});

gulp.task('build', function(callback) {
  runSequence('clean', 'html:critical', callback);
});

gulp.task('default', ['watch']);
