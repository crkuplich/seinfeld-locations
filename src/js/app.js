/*!
 * Copyright 2017 Cassiano Rocha Kuplich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Hardcoded locations data.
var locations = [
  {
    title: "Tom's Restaurant",
    location: { lat: 40.8054303, lng: -73.9654035 },
    marker: null
  },
  {
    title: 'H&H Bagels',
    location: { lat: 40.774433, lng: -73.95448 },
    marker: null
  },
  {
    title: 'The Soup Nazi',
    location: { lat : 40.765429, lng : -73.983264 },
    marker: null
  },
  {
    title: 'Mount Sinai Roosevelt',
    location: { lat: 40.76966, lng: -73.98679 },
    marker: null
  },
  {
    title: "Gray's Papaya",
    location: { lat: 40.7783771, lng: -73.9815748 },
    marker: null
  },
  {
    title: "Sotheby's",
    location: { lat: 40.7664162, lng: -73.9538446 },
    marker: null
  },
  {
    title: "Mendy's",
    location: { lat: 40.7473632, lng: -73.9815956 },
    marker: null
  },
  {
    title: "Pete's Tavern",
    location: { lat: 40.73653, lng: -73.986746 },
    marker: null
  },
  {
    title: 'Rockaway Beach',
    location: { lat: 40.5801121, lng: -73.8291844 },
    marker: null
  },
  {
    title: 'East River State Park',
    location: { lat: 40.72177, lng: -73.96219 },
    marker: null
  },
  {
    title: 'New York Public Library',
    location: { lat: 40.75325, lng: -73.98226 },
    marker: null
  },
  {
    title: 'Yankee Stadium',
    location: { lat: 40.82962, lng: -73.92650 },
    marker: null
  },
  {
    title: 'The Nexus of the Universe',
    location: { lat: 40.72327, lng: -73.98846 },
    marker: null
  },
  {
    title: "Jerry's Apartment",
    location: { lat: 40.78383, lng: -73.97536 },
    marker: null
  },
  {
    title: "Elaine's Apartment",
    location: { lat: 40.7973047, lng: -73.9607502 },
    marker: null
  },
  {
    title: "George's Apartment",
    location: { lat: 40.792, lng: -73.97677 },
    marker: null
  },
  {
    title: "The Costanza's House",
    location: { lat: 40.77291, lng: -73.90829 },
    marker: null
  }
];

// ViewModel to manage app actions.
var AppViewModel = function(locations, map) {
  var self = this;

  self.filter = ko.observable();
  self.navigationVisible = ko.observable(false);
  self.aboutOpen = ko.observable(false);
  self.locations = locations;
  self.map = map;
  self.popup = L.popup();

  var bounds = L.latLngBounds();
  self.locations.forEach(function(location) {
    // Create a marker and add to the map.
    var marker = L.marker(location.location)
      .bindPopup(self.popup)
      .addTo(self.map);
    marker.on('click', function(evt) {
      selectMarker(location);
    });
    // Add the marker to the current location object.
    location.marker = marker;
    // Add current location to the bounds object.
    bounds.extend(location.location);
  });
  // Show the map in a way where all markers appear.
  self.map.fitBounds(bounds);

  // Returns the locations filtered by self.filter() property.
  self.filteredLocations = ko.computed(function() {
    if (!self.filter()) {
      self.locations.forEach(function(location) {
        location.marker.addTo(self.map);
      });
      return self.locations;
    }
    return ko.utils.arrayFilter(self.locations, function(location) {
      var matches = location.title.toLowerCase().includes(self.filter().toLowerCase());
      if (matches) {
        location.marker.addTo(self.map);
      } else {
        location.marker.remove();
      }
      return matches;
    });
  });

  // Toggles navigation sidebar.
  self.toggleNavigation = function() {
    self.closeAbout();
    self.navigationVisible(!self.navigationVisible());
  };

  // Opens information about the app.
  self.openAbout = function() {
    self.aboutOpen(true);
  };

  // Closes information about the app.
  self.closeAbout = function() {
    self.aboutOpen(false);
  };

  // Selects a location.
  self.selectLocation = function(location) {
    self.navigationVisible(false);
    selectMarker(location);
  };

  // Helper function to get the Wikipedia articles related to a location.
  var getWikipediaArticles = function(location) {
    $.ajax({
      url: 'https://en.wikipedia.org/w/api.php',
      data: {
        format: 'json',
        formatversion: 2,
        action: 'query',
        generator: 'search',
        gsrsearch: '"' + location.title + '"',
        gsrlimit: 5,
        prop: 'info',
        inprop: 'url',
        redirects: true
      },
      dataType: 'jsonp'
    }).done(function(response) {
      var heading = '<h2 class="loc-info-title">' + location.title + '</h2>';
      var paragraph = '<p class="loc-info-text">';
      var urlList = '';

      // When response contains "query" and "pages" property, it contains at least one page.
      if (response.query && response.query.pages) {
        paragraph += '<strong>Wikipedia</strong> articles related to <em>' + location.title + '</em>:</p>';

        urlList = '<ul class="loc-article-list">';
        response.query.pages.forEach(function(page) {
          urlList += '<li><a href="' + page.fullurl + '" target="_blank" ' +
            ' rel="noopener noreferrer">' + page.title + '</a></li>';
        });
        urlList += '</ul>';
      } else { // otherwise, response doesn't contain any page.
        paragraph += 'No <strong>Wikipedia</strong> articles found for this location.</p>';
      }

      self.popup.setContent(heading + paragraph + urlList).update();
    }).fail(function(response) {
      console.log('Error! Status: ' + response.status);

      var content = '<h2 class="loc-info-title">Error!</h2>' +
        '<p class="loc-info-text">An error prevented Wikipedia articles from being retrieved.</p>';
      self.popup.setContent(content).update();
    });
  };

  // Helper function called when a marker is selected.
  var selectMarker = function(location) {
    self.popup.setContent('Loading articles...');
    location.marker.openPopup();
    getWikipediaArticles(location);
  };
};

var initApp = function() {
  // Init the map.
  var newYork = [40.7127, -74.0059];
  var map = L.map('map').setView(newYork, 14);

  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors'
  }).addTo(map);

  // Apply AppViewModel data bindings.
  ko.applyBindings(new AppViewModel(locations, map));
};

// Helper function called when an error occurs on loading the map.
var mapError = function() {
  alert('An error has ocurred when loading the map!');
};
