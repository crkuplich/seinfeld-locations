# Seinfeld Locations

[Seinfeld Locations](https://crkuplich.gitlab.io/seinfeld-locations/) is a map
with locations featured in _Seinfeld_, the show about nothing. Each location
marked on the map presents a short list of their related articles from
[Wikipedia](https://en.wikipedia.org/).

The addresses of all locations were obtained from [this Roadtrippers
article](https://roadtrippers.com/stories/a-seinfeld-filming-locations-guide-to-new-york-city),
except for Jerry's apartment, which was obtained from [this WikiSein
article](http://seinfeld.wikia.com/wiki/Jerry's_Apartment).

## Development

- [Download and install Node.js](https://nodejs.org/en/download/) if it isn't
  already installed in your system.
- Open a terminal and install [gulp](http://gulpjs.com/) globally:

  ```
  $ npm install -g gulp
  ```

- Clone the repository:

  ```
  $ git clone https://gitlab.com/crkuplich/seinfeld-locations.git [project-directory]
  ```

- Enter the project root directory and install its development dependencies:

  ```
  $ cd <project-directory>
  $ npm install
  ```

- Run the following command to list the available gulp tasks:

  ```
  $ gulp -T
  ```

- The default gulp task starts a local development web server on `src/`
  directory (source files) and watches for any file modification in this
  directory, reloading the application automatically. Just type:

  ```
  $ gulp
  ```

- When the application is ready to release, run `gulp build` to optimize the
  HTML, CSS and JS files for production. The files will be in the `public/`
  directory.

  ```
  $ gulp build
  ```

  You can also serve the files in `public/` to verify that all is ok:

  ```
  $ gulp serve:public
  ```

## Contributing

Please read the [Contribution Guide](CONTRIBUTING.md) for instructions about how
to contribute to the project.

## License

Copyright 2017 Cassiano Rocha Kuplich

All files of this application are licensed under the [Apache License, Version
2.0](http://www.apache.org/licenses/LICENSE-2.0), except for the following files
at `src/js/lib` directory:

- `jquery-3.1.1.min.js` is licensed by [jQuery
  license](https://github.com/jquery/jquery/blob/master/LICENSE.txt)
- `knockout-3.4.1.js` is licensed by [Knockout
  license](https://github.com/knockout/knockout/blob/master/LICENSE)

## Disclaimer

All copyrights and trademarks for "Seinfeld" are the property of their
respective owners.
